<?php

namespace credy\requestbodycachenator;

use yii\base\Behavior;
use yii\base\Controller;
use yii\caching\Cache;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\web\Request;

class CacheParameterBehavior extends Behavior
{
    /**
     * @var Request|array|string
     */
    public $request = 'request';

    /**
     * @var Cache|array|string
     */
    public $cache = 'cache';

    /**
     * @var integer
     */
    public $cacheDuration;

    /**
     * @var array
     */
    public $filterParameters = [];

    /**
     * @var callable
     */
    public $cacheKey;

    public function events()
    {
        return [
            Controller::EVENT_AFTER_ACTION => 'cacheParameterEvent',
        ];
    }

    public function init()
    {
        $this->cache = Instance::ensure($this->cache, Cache::class);
        $this->request = Instance::ensure($this->request, Request::class);
    }

    public function cacheParameterEvent()
    {
        $requestData = $this->request->isPost ? $this->request->bodyParams : $this->request->queryParams;
        $cacheId = call_user_func($this->cacheKey);

        foreach ($this->filterParameters as $parameter) {
            unset($requestData[$parameter]);
        }

        $previousValue = $this->cache->get($cacheId);

        if (!is_array($previousValue)) {
            $previousValue = [];
        }
        $this->cache->set($cacheId, array_unique(ArrayHelper::merge($this->processData($requestData), $previousValue)), $this->cacheDuration);
    }

    private function processData($array, $prefix = '')
    {
        $result = [];
        foreach ($array as $key => $value) {
            $result[] = $prefix . $key;
            if (is_array($value)) {
                $result = ArrayHelper::merge($result, $this->processData($value, $prefix . $key . '.'));
            }
        }
        return $result;
    }
}
