<?php

namespace tests\unit;

use Codeception\Stub\Expected;
use Codeception\Test\Unit;
use credy\requestbodycachenator\CacheParameterBehavior;
use yii\base\Controller;
use yii\base\InvalidConfigException;
use yii\caching\ArrayCache;
use yii\web\Request;

class CacheParameterBehaviorTest extends Unit
{
    /**
     * @test
     */
    public function testCacheInitializing()
    {
        $this->expectException(InvalidConfigException::class);
        $this->expectExceptionMessage('Invalid data type: yii\web\Request. yii\caching\Cache is expected.');

        new CacheParameterBehavior([
            'request' => new Request(),
            'cache' => new Request(),
        ]);
    }

    /**
     * @test
     */
    public function testRequestInitializing()
    {
        $this->expectException(InvalidConfigException::class);
        $this->expectExceptionMessage('Invalid data type: yii\caching\ArrayCache. yii\web\Request is expected.');

        new CacheParameterBehavior([
            'request' => new ArrayCache(),
            'cache' => new ArrayCache(),
        ]);
    }

    /**
     * @test
     */
    public function testFilterParameters()
    {
        $request = new Request([
            'queryParams' => [
                'filter1' => 'filterVal1',
                'filter2' => 'filterVal2',
                'param1' => 'value1',
            ],
        ]);
        $cache = new ArrayCache();

        $behavior = new CacheParameterBehavior([
            'request' => $request,
            'cache' => $cache,
            'cacheKey' => function () {
                return 'test';
            },
            'filterParameters' => ['filter1', 'filter2'],
        ]);
        $behavior->cacheParameterEvent();

        $params = $cache->get('test');
        $this->assertEquals(['param1'], $params);
    }

    /**
     * @test
     */
    public function testCacheKey()
    {
        $request = new Request([
            'queryParams' => [
                'param1' => 'value1',
            ],
        ]);
        $cache = new ArrayCache();

        $behavior = new CacheParameterBehavior([
            'request' => $request,
            'cache' => $cache,
            'cacheKey' => function () {
                return 'test2';
            },
        ]);
        $behavior->cacheParameterEvent();

        $this->assertFalse($cache->get('test'));
        $params = $cache->get('test2');
        $this->assertEquals(['param1'], $params);
    }

    /**
     * @test
     */
    public function testEvents()
    {
        $request = new Request();
        $cache = new ArrayCache();
        $behavior = new CacheParameterBehavior([
            'request' => $request,
            'cache' => $cache,
        ]);
        $value = $behavior->events();

        $this->assertIsArray($value);
        $this->assertArrayHasKey(Controller::EVENT_AFTER_ACTION, $value);
        $this->assertIsCallable([$behavior, $value[Controller::EVENT_AFTER_ACTION]]);
    }

    /**
     * @test
     */
    public function testParameterAddedToCache()
    {
        $request = new Request([
            'queryParams' => [
                'param1' => 'value1',
            ],
        ]);
        $cache = new ArrayCache();

        $behavior = new CacheParameterBehavior([
            'request' => $request,
            'cache' => $cache,
            'cacheKey' => function () {
                return 'test';
            }
        ]);
        $behavior->cacheParameterEvent();

        $params = $cache->get('test');
        $this->assertEquals(['param1'], $params);
    }

    /**
     * @test
     */
    public function testMultipleParametersAddedToCache()
    {
        $request = new Request([
            'queryParams' => [
                'param1' => 'value1',
                'param2' => 'value2',
            ],
        ]);
        $cache = new ArrayCache();

        $behavior = new CacheParameterBehavior([
            'request' => $request,
            'cache' => $cache,
            'cacheKey' => function () {
                return 'test';
            }
        ]);
        $behavior->cacheParameterEvent();

        $params = $cache->get('test');
        $this->assertEquals(['param1', 'param2'], $params);
    }

    /**
     * @test
     */
    public function testDifferentRequestParametersAddedToCache()
    {
        $request = new Request([
            'queryParams' => [
                'param1' => 'value1',
            ],
        ]);
        $cache = new ArrayCache();
        $cache->add('test', ['param2']);

        $behavior = new CacheParameterBehavior([
            'request' => $request,
            'cache' => $cache,
            'cacheKey' => function () {
                return 'test';
            }
        ]);
        $behavior->cacheParameterEvent();

        $params = $cache->get('test');
        $this->assertEquals(['param1', 'param2'], $params);
    }

    /**
     * @test
     */
    public function testDifferentRequestsDuplicateParametersAddedToCache()
    {
        $request = new Request([
            'queryParams' => [
                'param1' => 'value1',
            ],
        ]);
        $cache = new ArrayCache();
        $cache->add('test', ['param1']);

        $behavior = new CacheParameterBehavior([
            'request' => $request,
            'cache' => $cache,
            'cacheKey' => function () {
                return 'test';
            }
        ]);
        $behavior->cacheParameterEvent();

        $params = $cache->get('test');
        $this->assertEquals(['param1'], $params);
    }

    /**
     * @test
     */
    public function testArrayParametersAddedToCache()
    {
        $request = new Request([
            'queryParams' => [
                'param1' => [
                    'param2' => 'value2',
                    'param3' => 'value3',
                ],
            ],
        ]);
        $cache = new ArrayCache();
        $cache->add('test', ['param1']);

        $behavior = new CacheParameterBehavior([
            'request' => $request,
            'cache' => $cache,
            'cacheKey' => function () {
                return 'test';
            }
        ]);
        $behavior->cacheParameterEvent();

        $params = $cache->get('test');
        $this->assertEquals(['param1', 'param1.param2', 'param1.param3'], $params);
    }

    /**
     * @test
     */
    public function testPostParametersAddedToCache()
    {
        $request = $this->construct(Request::class, [[
            'bodyParams' => [
                'param1' => 'value1',
            ],
        ]], [
            'getMethod' => function () {
                return 'POST';
            }
        ]);

        $cache = new ArrayCache();

        $behavior = new CacheParameterBehavior([
            'request' => $request,
            'cache' => $cache,
            'cacheKey' => function () {
                return 'test';
            }
        ]);
        $behavior->cacheParameterEvent();

        $params = $cache->get('test');
        $this->assertEquals(['param1'], $params);
    }

    /**
     * @test
     */
    public function testCacheDuration()
    {
        $cache = $this->construct(ArrayCache::class, [], [
            'setValue' => Expected::once(function ($key, $value, $duration) {
                $this->assertEquals(60, $duration);
            })
        ]);

        $behavior = new CacheParameterBehavior([
            'request' => new Request(),
            'cache' => $cache,
            'cacheDuration' => 60,
            'cacheKey' => function () {
                return 'test';
            }
        ]);
        $behavior->cacheParameterEvent();
    }
}
